import  * as Express from "express";
import * as bodyParser from 'body-parser';
import { v4 as uuidv4 } from 'uuid';
import { Pad } from './models/Pad';

export function configureRest(app: Express.Express, pads: Pad[]) {
    const cors = require('cors');
    const clientUrl = process.env.NODE_ENV == 'production' ? 'https://cryptopad.gaboratorium.com' : 'http://localhost:4200';
    const corsOptions = { origin: clientUrl, optionsSuccessStatus: 200 };

    app.use(cors(corsOptions));
    app.use(bodyParser.urlencoded());
    app.use(bodyParser.json())

    app.post('/pads', (req: Express.Request, res: Express.Response) => {
        if (req.body['content'] == null) {
            res.status(400).send();
        } else {
            const id: string = uuidv4();
            const pad: Pad = {id: id, content: req.body['content']};
            pads.push(pad);
            res.setHeader('Content-Type', 'application/json');
            res.send(pad);
        }
    });

    app.get('/pads/:id', (req: Express.Request, res: Express.Response) => {
        const pad: Pad = pads.find(currentPad => currentPad.id == req.params['id']);
        if (pad) {
            res.setHeader('Content-Type', 'application/json');
            res.send(pad);
        } else {
            return res.sendStatus(404);
        }
    });
}