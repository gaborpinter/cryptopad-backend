import * as Socket from 'socket.io';

export interface ClientConnection {
    socket: Socket.Socket,
    padId: string,
    clientSecret: string
}