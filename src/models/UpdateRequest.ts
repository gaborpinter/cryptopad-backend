import { Pad } from './Pad'
import { WordArray } from 'crypto-js';

export interface UpdateRequest {
    pad: Pad;
    hmac: string;
}