import  * as Express from "express";
import * as SocketIO from 'socket.io';
import * as Http from 'http';
import { configureRest } from './rest'
import { configureSocket } from './socket'
import { Pad } from './models/Pad';

const app: Express.Express = require('express')();
const http: Http.Server = require('http').Server(app);
const io: any = require('socket.io')(http);
const port = process.env.PORT || 4000;
const pads:Pad[] =[];

configureRest(app, pads);
configureSocket(io, pads);
http.listen(port);


