import { UpdateRequest } from './models/UpdateRequest';
import { ClientConnection } from './models/ClientConnection'
import  * as Socket from 'socket.io';
import { Pad } from './models/Pad';
import * as CryptoJS from 'crypto-js';

export function configureSocket(io: any, pads: Pad[]) {
    const clientConnections: ClientConnection[] = [];

    io.on('connection', (socket: Socket.Socket) => initializeSocketConnection(socket));

    const initializeSocketConnection = (socket: Socket.Socket) => {

        socket.on('padUpdateFromClient', (request: UpdateRequest) => {
            const computedHash = hash(request.pad.content, clientSecret);
            if (computedHash === request.hmac) {
                const pad: Pad = updatePad(request);
                broadcastUpdate(pad);
            } else {
                console.log("Error: hash mismatch when updating Pad from client.")
            }
        });

        const updatePad = (updateRequest: UpdateRequest): Pad => {
            const padToBeUpdated = pads.find(pad => pad.id == updateRequest.pad.id);
            const indexToBeUpdated = pads.indexOf(padToBeUpdated);
            pads[indexToBeUpdated] = updateRequest.pad;
            return pads[indexToBeUpdated];
        }

        const broadcastUpdate = (pad: Pad) => {
            clientConnections.forEach((connection) => {
                if (connection.padId == pad.id) {
                    const hmac = hash(pad.content, connection.clientSecret);
                    const updateRequestFromServer = { pad, hmac }        
                    connection.socket.emit('padUpdateFromServer', updateRequestFromServer);
                }
            });
        }

        let previousId: string;
        let clientSecret: string;

        socket.on('joinRequestFromClient', (padId: string, newClientSecret: string) => {
            socket.leave(previousId);
            socket.join(padId);
            clientSecret = newClientSecret;
            previousId = padId;

            clientConnections.push({socket, padId, clientSecret});

            const pad = pads.find(pad => pad.id == padId);
            const hmac = hash(pad.content, clientSecret);
            const updateRequest: UpdateRequest = { pad, hmac };

            socket.emit('padUpdateFromServer', updateRequest);
        });
    }

    const hash = (plainText: string, secret: string): string => {
        return CryptoJS.HmacSHA1(plainText, secret).toString(CryptoJS.enc.Hex);
    }
}